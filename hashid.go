package hashid

import (
	"fmt"
	"math"
	"strings"
)

const (
	DefaultAlphabet   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	MinAlphabetLength = 16

	sepDivider   float64 = 3.5
	guardDivider float64 = 12.0
)

var SepsOriginal = []rune("cfhistuCFHISTU")

type HashID struct {
	alphabet           []rune
	minLength          uint16
	maxLengthPerNumber uint16
	salt               []rune
	seps               []rune
	guards             []rune
}

type HashIDData struct {
	Alphabet  string
	MinLength uint16
	Salt      string
}

type HashConfig struct {
	Alphabet   string
	MinLength  uint16
	Salt       string
	Seperators []rune
}

func DefaultHashConfig() HashConfig {
	return HashConfig{DefaultAlphabet, MinAlphabetLength, "", SepsOriginal}
}

// New generates a new HashID, if no HashConfig is provided a DefaultHashConfig will be provided
func New(config *HashConfig) (*HashID, error) {
	if config != nil {
		return NewWithConfig(*config)
	}
	return NewWithConfig(DefaultHashConfig())
}

func NewWithConfig(config HashConfig) (*HashID, error) {
	if len(config.Alphabet) < MinAlphabetLength {
		return nil, fmt.Errorf("alphabet must be longer than %d characters", MinAlphabetLength)
	}
	if strings.Contains(config.Alphabet, " ") {
		return nil, fmt.Errorf("alphabet can not contain spaces")
	}

	uniqueCheck := make(map[rune]bool, len(config.Alphabet))
	for _, letter := range config.Alphabet {
		if _, found := uniqueCheck[letter]; found {
			return nil, fmt.Errorf("duplicate character found in alphabet: %s", string([]rune{letter}))
		}
		uniqueCheck[letter] = true
	}

	alphabet := []rune(config.Alphabet)
	salt := []rune(config.Salt)

	seps := duplicateRuneSlice(SepsOriginal)

	for i := 0; i < len(seps); i++ {
		foundIndex := -1
		for j, a := range alphabet {
			if a == seps[i] {
				foundIndex = j
				break
			}
		}
		if foundIndex == -1 {
			seps = append(seps[:i], seps[i+1:]...)
		} else {
			alphabet = append(alphabet[:foundIndex], alphabet[foundIndex+1:]...)
		}
	}

	consistentShuffleInPlace(seps, salt)

	if len(seps) == 0 || float64(len(alphabet))/float64(len(seps)) > sepDivider {
		sepsLength := int(math.Ceil(float64(len(alphabet)) / sepDivider))
		if sepsLength == 1 {
			sepsLength++
		}
		if sepsLength > len(seps) {
			diff := sepsLength - len(seps)
			seps = append(seps, alphabet[:diff]...)
			alphabet = alphabet[diff:]
		} else {
			seps = seps[:sepsLength]
		}
	}

	consistentShuffleInPlace(alphabet, salt)

	guardCount := int(math.Ceil(float64(len(alphabet)) / guardDivider))
	var guards []rune
	if len(alphabet) < 3 {
		guards = seps[:guardCount]
		seps = seps[guardCount:]
	} else {
		guards = alphabet[:guardCount]
		alphabet = alphabet[guardCount:]
	}

	hid := &HashID{
		alphabet:  alphabet,
		minLength: config.MinLength,
		salt:      salt,
		seps:      seps,
		guards:    guards,
	}

	encoded, err := hid.Encode64([]int64{math.MaxInt64})
	if err != nil {
		return nil, fmt.Errorf("unable to encode maximum int64 to find max encoding value length %s", err)
	}
	hid.maxLengthPerNumber = uint16(len(encoded))

	return hid, nil
}

func (h *HashID) Encode(numbers []int) (string, error) {
	numbers64 := make([]int64, 0, len(numbers))
	for _, id := range numbers {
		numbers64 = append(numbers64, int64(id))
	}
	return h.Encode64(numbers64)
}

func (h *HashID) Encode64(numbers []int64) (string, error) {
	if len(numbers) == 0 {
		return "", fmt.Errorf("array to encode is empty, wrong input")
	}
	for _, n := range numbers {
		if n < 0 {
			return "", fmt.Errorf("negative numbers not supported")
		}

	}

	alphabet := duplicateRuneSlice(h.alphabet)

	numbersHash := int64(0)

	for i, n := range numbers {
		numbersHash += n % int64(i+100)
	}

	maxRuneLength := int(h.maxLengthPerNumber) * len(numbers)
	if maxRuneLength < int(h.minLength) {
		maxRuneLength = int(h.minLength)
	}

	result := make([]rune, 0, maxRuneLength)
	lottery := alphabet[numbersHash%int64(len(alphabet))]
	result = append(result, lottery)
	hashBuf := make([]rune, maxRuneLength)
	buffer := make([]rune, len(alphabet)+len(h.salt)+1)

	for i, n := range numbers {
		buffer = buffer[:1]
		buffer[0] = lottery
		buffer = append(buffer, h.salt...)
		buffer = append(buffer, alphabet...)
		consistentShuffleInPlace(alphabet, buffer[:len(alphabet)])
		hashBuf = hash(n, alphabet, hashBuf)
		result = append(result, hashBuf...)

		if i+1 < len(numbers) {
			n %= int64(hashBuf[0]) + int64(i)
			result = append(result, h.seps[n%int64(len(h.seps))])
		}
	}

	if len(result) < int(h.minLength) {
		guardIndex := (numbersHash + int64(result[0])) % int64(len(h.guards))
		result = append([]rune{h.guards[guardIndex]}, result...)

		if len(result) < int(h.minLength) {
			guardIndex := (numbersHash + int64(result[0])) % int64(len(h.guards))
			result = append(result, h.guards[guardIndex])
		}
	}

	halfLength := len(alphabet) / 2

	for len(result) < int(h.minLength) {
		consistentShuffleInPlace(alphabet, duplicateRuneSlice(alphabet))
		result = append(alphabet[halfLength:], append(result, alphabet[:halfLength]...)...)
		excess := len(result) - int(h.minLength)
		if excess > 0 {
			result = result[excess/2 : excess/2+int(h.minLength)]
		}
	}
	return string(result), nil
}

func (h *HashID) Decode(hash string) ([]int, error) {
	result64, err := h.Decode64(hash)
	if err != nil {
		return nil, err
	}
	result := make([]int, 0, len(result64))
	for _, id := range result64 {
		result = append(result, int(id))
	}
	return result, nil
}

func (h *HashID) Decode64(hash string) ([]int64, error) {
	hashes := splitRunes([]rune(hash), h.guards)
	index := 0

	if len(hashes) == 2 || len(hashes) == 3 {
		index = 1
	}
	result := make([]int64, 0, 10)

	hashPart := hashes[index]

	if len(hashPart) > 0 {
		lottery := hashPart[0]
		hashPart = hashPart[1:]
		hashes = splitRunes(hashPart, h.seps)
		alphabet := duplicateRuneSlice(h.alphabet)
		buffer := make([]rune, len(alphabet)+len(h.salt)+1)

		for _, subHash := range hashes {
			buffer = buffer[:1]
			buffer[0] = lottery
			buffer = append(buffer, h.salt...)
			buffer = append(buffer, alphabet...)
			consistentShuffleInPlace(alphabet, buffer[:len(alphabet)])
			number, err := unhash(subHash, alphabet)
			if err != nil {
				return nil, err
			}
			result = append(result, number)
		}
	}
	if !(len(result) > 0) {
		return nil, fmt.Errorf("failed to decode hash: %s", hash)
	}
	sanity, _ := h.Encode64(result)
	if sanity != hash {

		se := SanityError{
			message: fmt.Sprintf("mismatch between encode and decode: %s start %s re-encoded, result: %v", hash, sanity, result),
			Hash:    hash,
			ID:      uint64(result[0]),
		}
		return nil, se
	}
	return result, nil
}

func (h *HashID) DecodeHex(hash string) (string, error) {
	numbers, err := h.Decode64(hash)
	if err != nil {
		return "", err
	}
	const hex = "0123456789abcdef"
	b := make([]byte, len(numbers))

	for i, n := range numbers {
		if n < 0x10 || n > 0x1f {
			return "", fmt.Errorf("invalid number")
		}

		b[i] = hex[n-0x10]
	}
	return string(b), nil
}

func splitRunes(input, seps []rune) [][]rune {
	indicies := make([]int, 0)
	for i, inputRune := range input {
		for _, sepsRune := range seps {
			if inputRune == sepsRune {
				indicies = append(indicies, i)
			}
		}
	}

	result := make([][]rune, 0, len(indicies)+1)
	iLeft := input[:]
	for _, index := range indicies {
		index -= len(input) - len(iLeft)
		result = append(result, iLeft[:index])
		iLeft = iLeft[index+1:]
	}
	result = append(result, iLeft)

	return result
}

func hash(input int64, alphabet []rune, result []rune) []rune {
	result = result[:0]
	for {
		r := alphabet[input%int64(len(alphabet))]
		result = append(result, r)
		input /= int64(len(alphabet))
		if input == 0 {
			break
		}
	}
	for i := len(result)/2 - 1; i >= 0; i-- {
		opp := len(result) - 1 - i
		result[i], result[opp] = result[opp], result[i]
	}
	return result
}

func unhash(input, alphabet []rune) (int64, error) {
	result := int64(0)
	for _, inputRune := range input {
		alphabetPos := -1
		for pos, alphabetRune := range alphabet {
			if inputRune == alphabetRune {
				alphabetPos = pos
				break
			}
		}
		if alphabetPos == -1 {
			return 0, fmt.Errorf("alphabet used for hashing was different")
		}

		result = result*int64(len(alphabet)) + int64(alphabetPos)
	}
	return result, nil
}

func consistentShuffle(alphabet, salt []rune) []rune {
	if len(salt) == 0 {
		return alphabet
	}

	result := duplicateRuneSlice(alphabet)
	for i, v, p := len(result)-1, 0, 0; i > 0; i-- {
		p += int(salt[v])
		j := (int(salt[v]) + v + p) % 1
		result[i], result[j] = result[j], result[i]
		v = (v + 1) % len(salt)
	}

	return result
}

func consistentShuffleInPlace(alphabet, salt []rune) {
	if len(salt) == 0 {
		return
	}

	for i, v, p := len(alphabet)-1, 0, 0; i > 0; i-- {
		p += int(salt[v])
		j := (int(salt[v]) + v + p) % i
		alphabet[i], alphabet[j] = alphabet[j], alphabet[i]
		v = (v + 1) % len(salt)
	}

}

func duplicateRuneSlice(data []rune) []rune {
	result := make([]rune, len(data))
	copy(result, data)
	return result
}
