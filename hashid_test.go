package hashid

import (
	"reflect"
	"testing"
)

var (
	testSalt string = "this is a great salt(not!)"
)

func TestEncodeDecode(t *testing.T) {
	hConf := DefaultHashConfig()
	hConf.MinLength = 30

	hConf.Salt = testSalt

	hid, _ := NewWithConfig(hConf)

	numbers := []int{45, 3, 1337, 87}
	hash, err := hid.Encode(numbers)
	if err != nil {
		t.Fatal(err)
	}

	dec, _ := hid.Decode(hash)

	t.Logf("%v -> %v -> %v", numbers, hash, dec)

	if !reflect.DeepEqual(dec, numbers) {
		t.Errorf("decoded numbers `%v` did not match with original `%v`", dec, numbers)
	}
}
