package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/fp-studio-hq/libraries/hashid-go"
)

var (
	major        = 0
	minor        = 1
	patch        = 0
	revision     = ""
	version      = fmt.Sprintf("%d.%d.%d-%s", major, minor, patch, revision)
	compiledSalt = ""
)

func main() {
	app := &cli.App{
		Name:     "hashid",
		Usage:    "converts numbers to deterministic hash values",
		Version:  version,
		Compiled: time.Now(),
		Authors: []*cli.Author{
			{
				Name:  "Marcus Olofsson",
				Email: "marcus@fp-studio.se",
			},
		},
		Copyright: "(c) 2022 Footprint Studio",
		HelpName:  "hashid",
		UsageText: "hashid - converts numbers to deterministic hash values",
		Action: func(ctx *cli.Context) error {
			config := hashid.HashConfig{
				Alphabet:   hashid.DefaultAlphabet,
				MinLength:  uint16(ctx.Int("length")),
				Salt:       ctx.String("salt"),
				Seperators: hashid.SepsOriginal,
			}
			hashId, err := hashid.New(&config)
			if err != nil {
				fmt.Printf("failed to create HashID system: %x", err)
				os.Exit(1)
			}
			hash, err := hashId.Encode(ctx.IntSlice("ids"))
			if err != nil {
				fmt.Printf("error occured while hashing id: %x", err)
			}
			fmt.Println(fmt.Sprintf("hashid of value: %s", hash))
			return nil
		},
	}
	app.Flags = []cli.Flag{
		&cli.IntSliceFlag{
			Name:     "ids",
			Usage:    "Intergers to hash",
			FilePath: "",
			Value:    nil,
		},
		&cli.IntFlag{
			Name:    "length",
			Aliases: []string{"l"},
			Value:   12,
			Usage:   "The length of the hash to be produced",
		},
		&cli.StringFlag{
			Name:        "salt",
			Aliases:     []string{"s"},
			Usage:       "for using a specific salt",
			Value:       compiledSalt,
			DefaultText: "random string here",
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
