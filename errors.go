package hashid

type SanityError struct {
	message string
	Hash    string
	ID      uint64
}

func (se SanityError) Error() string {
	return se.message
}
